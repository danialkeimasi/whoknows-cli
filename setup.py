from setuptools import setup

setup(
    name='whonknows template manager',
    version='0.1',
    py_modules=['wtm'],
    install_requires=[
        'Click',
        'pprint',
        'requests',
        'pyyaml',
        'attrdict',
        'colorlog',
        'Click',
    ],

    entry_points='''
        [console_scripts]
        wtm=wtm:cli
    ''',
)
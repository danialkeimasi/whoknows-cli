import requests
from config.config import config
import json


def template_new(payload = {}):
    url = f'{config.server.address}{config.request.new}'
    response = requests.post(url, json=payload)
    return response.json()


def template_find(payload = {}):
    url = f'{config.server.address}{config.request.find}'
    response = requests.post(url, json=payload)
    return response.json()


def template_test_save(payload = {}):
    url = f'{config.server.address}{config.request.test_save}'
    response = requests.post(url, json=payload)
    try:
        return response.json()
    except Exception as e:
        print(f'error: {e}')
        print(response.text)
        exit(1)


def question_generate(payload = {}):
    url = f'{config.server.address}{config.request.generate}'
    response = requests.post(url, json=payload)
    return response.json()

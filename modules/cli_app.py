import click
import json
import os

from config.config import config, logger
from bson import json_util
from modules import api_requests, tools


@click.group()
def cli():
    pass


@cli.command()
@click.option('--idea', '-i', help=config.message.new.idea, prompt=config.message.new.idea)
def new(idea):
    res = api_requests.template_new({'idea': idea})
    template = res['template']

    file_address = tools.find_file_address('idea', res['_id']['$oid'], idea)
    tools.save_json(template, file_address)


@cli.command()
@click.argument('templatepath', type=click.Path(exists=True))
# @click.option('--template', '-t', required=True, type=click.File('r', encoding='utf-8'), help=config.message.all.template, prompt=config.message.all.template)
def test_save(templatepath):

    data = json.loads(open(templatepath, 'r', encoding='utf-8').read())

    request = {
        'template': data
    }
    response = api_requests.template_test_save(request)
    template = response['template']

    log = {"state": template["__state"], "test_log": template["__last_test"]}
    logger.warning(f'  {log}')
    template = response['template']

    if response['ok']:
        logger.info(f'template updated and saved to database.')

    file_address = tools.find_file_address(template['__state'], response['_id']['$oid'], template['__idea'])
    os.remove(templatepath)
    tools.save_json(template, file_address)

@cli.command()
@click.option('--tags', '-tg', multiple=True, help=config.message.find.tags)
@click.option('--ok', default=False, is_flag=True,  help=config.message.find.ok)
@click.option('--count', '-c', type=int, help=config.message.find.count)
@click.option('--query', '-q', help=config.message.find.query)
def find(tags, ok, count, query):
    query = data = json.loads(query, object_hook=json_util.object_hook) if query else query
    print(tags, ok, count, query)


@cli.command()
@click.option('--template', '-t', required=True, type=click.File('r', encoding='utf-8'), help=config.message.all.template, prompt=config.message.all.template)
def generate(template):
    data = json.loads(template.read(), object_hook=json_util.object_hook)

    print(data)
    print(type(data))



@cli.command()
@click.option('--rewrite', default=False, is_flag=True,  help=config.message.find.ok)
def clone(rewrite):

    logger.critical(f'making directory ...')

    main_path = list(config.path.keys())[0]
    paths = [os.path.join(main_path, subpath) for subpath in config.path.templates]

    for path in [main_path] + paths:
        if not os.path.exists(path):
            logger.info(f'make directory: {path}')
            os.mkdir(path)
        else:
            logger.warning(f'directory exists: {path}')

    logger.critical(f'cloning templates ...')

    templates = api_requests.template_find()['templates']
    for template in templates:

        file_address = tools.find_file_address(template['__state'], template['_id']['$oid'], template['__idea'])

        if rewrite or not os.path.isfile(file_address):
            tools.save_json(template, file_address)
        else:
            logger.warning(f'template file found.')


@cli.command()
def init():

    logger.critical(f'making directory ...')

    main_path = list(config.path.keys())[0]
    paths = [os.path.join(main_path, subpath) for subpath in config.path.templates]

    for path in [main_path] + paths:
        if not os.path.exists(path):
            logger.info(f'make directory: {path}')
            os.mkdir(path)
        else:
            logger.warning(f'directory exists: {path}')

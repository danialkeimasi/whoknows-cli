import os
import json
from bson import ObjectId

from config.config import config, logger

def find_dir(given_state):
    main_path = list(config.path.keys())[0]    
    states = [subpath[2:] for subpath in config.path.templates]
    states_folder_name = [subpath for subpath in config.path.templates]

    for i, state in enumerate(states):
        if given_state == state:
            return os.path.join(main_path, states_folder_name[i])
    else:
        return os.path.join(main_path, states_folder_name[0])

def find_file_address(template_state, template_id, template_idea):
    finded_dir = find_dir(template_state)
    datetime = ObjectId(template_id).generation_time
    return os.path.join(finded_dir, f"{datetime.date()}-{datetime.time()}_{template_idea[:20]}.json")


def save_json(template, file_address):
    file_address = file_address.replace(' ', '-')
    file_address = file_address.replace(':', '-')

    logger.info(f'writing template to file: {file_address}')
    json.dump(template, open(file_address, 'w+', encoding='utf-8'), indent=4, ensure_ascii=False)

import logging
import colorlog
import yaml
import attrdict
from pymongo import MongoClient
from log4mongo.handlers import MongoHandler
from pprint import pprint

config = attrdict.AttrDict(yaml.safe_load(open("./config/config.yml", 'r')))

logging.getLogger("matplotlib").setLevel(logging.WARNING)

stream_handler = colorlog.StreamHandler()
stream_handler.setFormatter(colorlog.ColoredFormatter())

logging.basicConfig(
    datefmt='%y-%b-%d %H:%M:%S',
    format='%(levelname)8s:[%(asctime)s][%(filename)20s:%(lineno)4s -%(funcName)20s() ]: %(message)s',

    # datefmt='%H:%M:%S',
    level=logging.DEBUG,
    handlers=[
        logging.FileHandler(f'{config.dir.project}/config/last_run.log', mode='w+', encoding='utf8', delay=0),
        stream_handler,
        # MongoHandler(host=config.mongo.ip, port=config.mongo.port,
        #              username=config.mongo.username, password=config.mongo.password,
        #              authentication_db=config.mongo.authentication_db, database_name='TemplateManager', collection='log'),
    ]
)
logger = logging.getLogger('TemplateEngine')
